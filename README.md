# GitIt

Initial contact with git (and GitLab). How to initiate your first repository, clone existing repositories, see changes, commit changes, revert changes, create and merge branches.

For more information, please go to the project Wiki.